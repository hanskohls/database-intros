# Intro

A few years ago, I read the book [7 databases in 7 weeks](https://pragprog.com/book/rwdata/seven-databases-in-seven-weeks) but I've forgotten a lot of it and whilst some of the databases introduced by the book did not take off, or I never needed them, there are so many new database systems that I wanted to learn about that I felt there is a use case to create a repository where I start to push data through different systems for my own reference. 

So here is the start ... an empty repository with just a README file. 
